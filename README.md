# blog
My personnal

## usage

https://gohugo.io/getting-started/quick-start/

New post:pos    -hcon   
`hugo new posts/my-first-post.md`

Load template (hugo-coder):
`git submodule update --init --recursive`

## Style

- console: https://terminalcss.xyz/

## Other tools

- https://favicon.io/favicon-converter/
- https://color.adobe.com/fr/create/color-wheel


## Tools

- https://app.grammarly.com/
- https://hemingwayapp.com/
- https://miro.com/app/
- https://imgflip.com/memegenerator
- https://asciiflow.com/#/