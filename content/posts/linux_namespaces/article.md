+++ 
draft = false
date = 2023-02-13T10:56:35+01:00
title = "How containers work — Linux namespaces"
description = "Experiment with the process namespace through the command line to better understand how containers work under the hood."
slug = "linux-namespaces"
authors = []
tags = ["Linux", "containers", "namespaces"]
categories = []
externalLink = ""
series = []
+++

Container technology has been too magical for me for too long. Now is the time to lift the curtain and see how it works under the hood. 
We will focus here on how containers isolate processes from the rest of the system. This is provided by the Linux kernel through the concept of namespaces.
The objective is to experiment with the process namespace through the command line. Let's go!

## What are namespaces?

Namespaces are a Linux kernel abstraction enabling the isolation of a set of resources from the rest of the system. Height namespaces are available:

* **PID**: process isolation. Each namespace has its own process tree. This is the one we will use in this article.
* **UTS**: hostname and domain name isolation.
* **IPC**: interprocess communication isolation (POSIX message queues and System V IPC objects)
* **Mount**: mount point isolation (filesystems).
* **Network**: network devices, stacks, ports etc. 
* **User**: user and group isolation.
* **Cgroup**: control group isolation.
* **Time**: boot and monotonic clock isolation.

How do processes relate to namespaces?

* A process can join several namespaces simultaneously (one per namespace type).
* A namespace can be joined by several processes at the same time.
* Several processes can share a disjoint set of namespaces (two processes can share the same PID namespace while having different user namespaces).

Another essential concept is the namespace lifetime. A namespace is destroyed when the last process in the namespace terminates, with some exceptions that you can check with `man 7 namespaces`.

## Checking namespaces

Namespaces can be observed in the `/proc/<pid>/ns` directory. It contains a pseudo filesystem of type `nsfs` where symlinks point to namespaces numbers. These namespaces numbers are the inode number pointed by the symlink. See the following commands:

```Bash
# List nemaspaces of the current process
$ ls -l /proc/$$/ns/
total 0
lrwxrwxrwx 1 root root 0 Feb 13 10:50 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 mnt -> 'mnt:[4026531841]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 net -> 'net:[4026531840]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 pid -> 'pid:[4026531836]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 time -> 'time:[4026531834]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 time_for_children -> 'time:[4026531834]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 user -> 'user:[4026531837]'
lrwxrwxrwx 1 root root 0 Feb 13 10:50 uts -> 'uts:[4026531838]'

# The inode number of the file reference by the symlink is the namespace identifier
$ ls -i -H /proc/$$/ns/user
4026531837 /proc/8213/ns/user
```

If two processes share the same inode number for a given namespace, they are in the same namespace.

There are limits to the number of namespaces that can be created. They are visible in the `/proc/sys/user/` directory:

```bash
$ find /proc/sys/user/* -type f -name '*namespaces' -print -exec cat {} \; | sed -z 's/s\n/s\t/g'
/proc/sys/user/max_cgroup_namespaces    15049
/proc/sys/user/max_ipc_namespaces       15049
/proc/sys/user/max_mnt_namespaces       15049
/proc/sys/user/max_net_namespaces       15049
/proc/sys/user/max_pid_namespaces       15049
/proc/sys/user/max_time_namespaces      15049
/proc/sys/user/max_user_namespaces      15049
/proc/sys/user/max_uts_namespaces       15049
```

# How to interact with namespaces — playing with the PID namespace

> **_NOTE:_** &#128039; Commands are run as root in the rest of the article.

They are a few useful commands to interact with namespaces:

* `unshare`: create a new namespace.
* `nsenter`: enter an existing namespace.
* `ip netns`: create and manage network namespaces.
* `lsns`: list namespaces.

### Creating a new namespace

We start by creating a new PID namespace. To do so, we use the `unshare` command. Run this command as a background process or you will not be able to run other commands (we'll see why later):

```bash
# Create a new PID namespace using
# --pid states that we want to create a new PID namespace
# --fork is needded so that a new process is created in the new namespace with PID 1
# --mount-proc creates a new mount namespace to mount a new /proc filesystem (ps command uses the /proc filesystem to get the list of processes)
$ unshare --pid --fork --mount-proc sleep 3600 &
[1] 8727

# Listing namespaces, we can see the mnt and pid namespaces
$ lsns
        NS TYPE   NPROCS   PID USER             COMMAND
...
4026532273 mnt         2  8727 root             unshare --pid --fork --mount-proc sleep 3600
4026532275 pid         1  8728 root             └─sleep 3600
```

We can see that the `unshare` command created new `pid` and `mnt` namespaces. The `pid` namespace is the one we are interested in. The `mnt` namespace is created to mount the `/proc` filesystem. 

We now have two processes (`unshare` and `sleep`) in two different `pid` namespaces, sharing the same `mnt` namespace. We can confirm it by looking at the `/proc` filesystem:

```bash
# Comparing the unshare and sleep processes: only the pid namespace is different
$ diff -y --suppress-common-lines <(ls -L -i -1 /proc/8727/ns/) <(ls -L -i -1 /proc/8728/ns/)
4026531836 pid                                                | 4026532275 pid

# Comparing the current process and the sleep process: both the pid and mnt namespaces are different
$ diff -y --suppress-common-lines <(ls -L -i -1 /proc/$$/ns/) <(ls -L -i -1 /proc/8728/ns/)
4026531841 mnt                                                | 4026532273 mnt
4026531836 pid                                                | 4026532275 pid
4026531836 pid_for_children                                   | 4026532275 pid_for_children
```

We can see that the `pid` namespace of the `unshare` process differs from the `pid` namespace of the `sleep` process. We also see that the current process and the `sleep` process have different `pid` and `mnt` namespaces.

`pid_for_children` file is a handle for the PID namespace of child processes created by this process. It is not of interest to us.

We can also check that only the `sleep` process has the `pid` namespace `4026532275` by searching files with the same inode number:

```bash
$ find -L /proc/*/ns -type f -samefile /proc/8728/ns/pid
/proc/8727/ns/pid_for_children
/proc/8728/ns/pid
/proc/8728/ns/pid_for_children
```

This confirms that the `sleep` process is the only process in the `pid` namespace 4026532275. 

If we do the same for the `mnt` namespace, we only find our two processes(`unshare` and `sleep`):

```bash
$ find -L /proc/*/ns -type f -samefile /proc/8728/ns/mnt
/proc/8727/ns/mnt
/proc/8728/ns/mnt
```

### Entering an existing namespace

We use `nsenter` to enter an existing namespace. We can use it to enter the `pid` namespace of the `sleep` process:

```bash
# use -a to also enter the mount namespace of the sleep process
$ nsenter -t 8728 -a
```

We can now see that we are in the `pid` namespace of the `sleep` process:

```bash
$ ps
    PID TTY          TIME CMD
      1 pts/2    00:00:00 sleep
      2 pts/2    00:00:00 bash
     12 pts/2    00:00:00 ps
```

This confirms that our view of the processes is limited to the `pid` namespace of the `sleep` process. While it has the PID 8728 in the parent namespace, it has the PID 1 in the child namespace.

The namespaces number view is the same as the parent namespace:

```bash
# Ran from the `sleep` process pid namespace, we share the same mnt and pid namespaces
$ ls -L -i -1 /proc/$$/ns
4026531835 cgroup
4026531839 ipc
4026532273 mnt
4026531840 net
4026532275 pid
4026532275 pid_for_children
4026531834 time
4026531834 time_for_children
4026531837 user
4026531838 uts
```

### The namespace init process

Now, let's try to kill the `sleep` process, from the same namespace:

```bash
# Kill the sleep process from the sleep process pid namespace
$ kill -9 1
$ ps 
    PID TTY          TIME CMD
      1 pts/2    00:00:00 sleep
      2 pts/2    00:00:00 bash
     15 pts/2    00:00:00 ps
```

Humm, it seems that the `sleep` process is still running. Looking at the documentation of `pid_namespaces`, we can see that the `sleep` process being the first process in the namespace, got the PID 1. This is the "init" process of the namespace, which is essential to the correct operation of the PID namespace. It thus does not get default signal handlers. If the process doesn't set its own signal handlers, nothing happens. Our kill signal is ignored. 

More precisely from the `man 7 pid_namespaces` documentation:

> Only signals for which the "init" process has established a signal handler can be sent to the "init" process by other members of the PID namespace.  This restriction applies even to privileged processes, and prevents other members of the PID namespace from accidentally killing the "init" process.

It can, however, be killed from the parent process:

> SIGKILL or SIGSTOP are treated exceptionally: these signals are forcibly delivered when sent from an ancestor PID namespace.  Neither of these signals can be caught by the "init" process, and so will result in the usual actions associated with those signals (respectively, terminating and stopping the process).

### Nested namespaces

Namespaces can be nested. Their PID numbers can be seen in the `/proc` filesystem:

```bash
# Create two nested namespaces
$ unshare --pid --fork --mount-proc bash -c 'unshare --pid --fork --mount-proc sleep 3600' &
[1] 9084

# Check the namespaces of the sleep process with PID 9085
$ cat /proc/9085/status | grep NSpid
NSpid:	9085 2 1
```

It shows that the sleep process has three PID namespaces: the current one (with PID 1), the parent one (with PID 2), and the grand-parent one (with PID 9085).

## Conclusion

The Linux kernel provides easy to work with abstractions for isolating resources with namespaces. This is one of the building blocks of containers on Linux. With this article, I hope I have faded away some of the containers inner workings magic. 

For writing this article, I have used the following resources:

* A great article from [Quarkslab](https://blog.quarkslab.com/digging-into-linux-namespaces-part-1.html) about all Linux namespaces.
* man 7 namespaces.
* man 7 pid_namespaces.

