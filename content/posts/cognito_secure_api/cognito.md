---
title: "Secure your APIs using AWS Cognito and Cloudformation"
date: 2022-10-11T13:53:59+02:00
draft: true
description: "Setup a basic but powerful Cognito configuration for securing a user-facing API for a public application."
slug: c19616a6-6db4-4b88-8b42-6ba5cccc69e7
authors: []
tags: ["AWS", "Cognito", "Cloudformation", "REST API", "OAuth 2.0", "OIDC", "Scopes", "Claims"]
categories: []
externalLink: ""
series: []
hidden: true
---


When building an HTTP Rest API, managing user authentication and authorization is a consistent hurdle. It is so common that many SaaS products exist to handle those for you. One is AWS Cognito, the AWS integrated solution that supports OpenID Connect (OIDC) and OAuth 2.0 protocols.

This article will show you how to set up a basic but powerful Cognito configuration for securing a public user-facing API using Cloudformation. 

As with many AWS services, it may seem challenging to understand and configure at first sight. Starting from the basics, we will build an actionable mental model so that you can quickly get up to speed and implement more specific configurations. 

To explore Cognito's configuration, we will apply it to a fake music streaming application use case.

--> Illustration

## Cognito, the AWS integrated Identity Provider

Why using it?

**It is cheap**

You pay for what you use. Pricing is based on Monthly Active Users (MAUs). Any operation on a user will make it active.

There is a very decent free tier of 50k MAUs. Beyond that, you will have to pay digressively from $0.0055 (at the time of writing this article) by MAU.

**It is serverless**

With serverless comes all its niceties like scalability, high availability, data durability, and security. You do not lose time operating this highly critical part of your infrastructure.

**It has lots of features**

Here is an image extracted from their [presentation page](https://aws.amazon.com/cognito/):

![Music app design](/posts/cognito_secure_api/cognito_product.png#medium)

Among the important features, we can cite:

* Integrated support of other identity providers: Google, Apple, Facebook, and Amazon. Or any OpenID Connect (OIDC)/SAML compatible Identity Provider;
* Multi-Factor Authentication (MFA) (supports SMS and token device). Remembers used devices;
* Security risk detection, check for compromised credentials;
* Provides a hosted UI you can customize so that it looks integrated into your app when using a web browser;
* Verifies user's attributes like the email and phone number, handles password reset;
* Behavior customization by plugging in lambdas to be called at different steps (such as PreAuthentication, PreSignUp...).

## Cloudformation for our deployment

Infrastructure as code is a best practice for such deployments. 

The industry often relies on Terraform, providing many desirable features like multi-cloud deployments and performant configuration language for describing your infrastructure. But for single cloud deployments, I feel that Cloudformation is underrated. Compared with Terraform, it carries downsides such as the YAML format (or JSON, but it is worse for that matter) and being vendor locked. However, being fully integrated with AWS, Cloudformation is more robust and reliable. Also, it does not rely on external state storage. 

We will use Cloudformation here, but the configuration can be easily transposed into Terraform.

## Building a music streaming API

The objective is to build an API for listening to music streaming, like Spotify. It will have an application for users, another for content creators, and the last one for administrators. It must also allow users to listen to their music from other third-party apps integrated with our API. Finally a public, more secured, admin access must be provided for managing the application.

In our very simple model, the API supports a single entity:

* Song: represents a song on the platform.

The API must support different types of users and accesses:

* Standard users can listen to a restricted set of songs;
* Premium users can listen to all songs;
* Content creators can access the listen and creator apps with the same account;
* Admins cannot sign up themselves, they must be created manually and their authentication require MFA for greater security.
* Third-party applications: can connect to the API on behalf of a user to play the user's music.

![Music app design](/posts/cognito_secure_api/music_app_design.jpg#medium)

With that in mind, let's explore Cognito's configuration and see how it can support our requirements.

## Cognito's components

Cloudformation configures Cognito using many objects. We study only the main ones to keep the article digestible and explain the Cognito's terminology.

Here is the schema of dependencies between all the objects that define the Cognito service in Cloudformation. 

![Cognito components](/posts/cognito_secure_api/cognito_components.jpg#medium)

For configuring Cognito, we must understand the following terminology:

* **User pools** are user directories that provide sign-up and sign-in options for your users;
* **Identity pools** provide AWS credentials to grant your users access to other AWS services. With an identity pool, your users can obtain temporary AWS credentials to access AWS services, such as Amazon S3 and DynamoDB. We don't need this object for our case; 
* **User Pool Client** is equivalent to a client application (or simply a client) within a user pool that has permission to call unauthenticated API operations (register, sign in, and handle forgotten passwords). You can create multiple applications for a given user pool. Each application has its own app client ID.
* **Id Provider** sets additional identity providers supported on top of Cognito. We will not be using it.

In a nutshell, and relevant to our case, we have:

* **User pools** to configure users' authentication, including: 
  * The attributes to use;
  * The attributes to verify (email, phone number); 
  * The MFA configuration (what type, required or optional).
* **User pool clients** to configure:
  * OAuth (auth flow, tokens validity, callback URLs, supported identity providers);
  * Authorizations (allowed scopes).

## Authentication configuration

Based on this, we know we will need two different user pools and three four Pool Clients:

![Cognito components](/posts/cognito_secure_api/user_pools.jpg#medium)

### Setting user pools

Starting with the user pool for standard users, we have the following cloudformation configuration:

```yaml
UserPoolName: music-api-user-pool
AutoVerifiedAttributes: # Verify the email
  - email
Schema:
  - Name: email
    AttributeDataType: String
    Mutable: false
    Required: true
UsernameAttributes: # Defines what users can log-in with as identifer.
  - email
```

Pretty straightforward. 

The configuration of the admin user pool adds some more user attributes, sets MFA and adds security:

```yaml
UserPoolName: music-api-admin-pool
# Admins cannot sign-up themselves, they have to be created 
# from the AWS console for example
AdminCreateUserConfig: 
  AllowAdminCreateUserOnly: true
AutoVerifiedAttributes:
  - email
# Sets token MFA. Tokens can be generates using applications 
# like the Google Authenticator.
EnabledMfas: 
  - SOFTWARE_TOKEN_MFA
MfaConfiguration: "ON" # MFA is required for all users
Schema:
  - Name: email
    AttributeDataType: String
    Mutable: false
    Required: true
  - Name: name
    AttributeDataType: String
    Mutable: true
    Required: true
  - Name: family_name
    AttributeDataType: String
    Mutable: true
    Required: true
UsernameAttributes: 
  - email
# Enables advanced security risk detection and
# audit logs to Cloudwatch
UserPoolAddOns: 
  AdvancedSecurityMode: AUDIT
```

### Setting the clients

A user Pool Client defines what identity providers (attached to the pool) can be used (Cognito is available by default) and configures OAuth.

<!-- Set the authorized identity providers for this user pool client:
SupportedIdentityProviders:
COGNITO is available by default
you can add other identity providers that you have attached to the user pool -->

The first part is about configuring the supported OAuth workflows. These are the available parameters:

* **AllowedOAuthFlows**: For our application, we should ideally use the code grant flow that prevents leaking out OAuth tokens. But it requires session handling on the backend side. In our example, we only set the implicit grant. In this case, the access token is provided directly to the client application, which must then include in the authorization header of every request.
* **ExplicitAuthFlows**: Again, here we should enable SRP authentication, but for our simple test, we discard it to be able to test it easily with cURL.
  * ALLOW_USER_PASSWORD_AUTH: Enable user password-based authentication. In this flow, Amazon Cognito receives the password in the request instead of using the SRP protocol to verify passwords.
  * ALLOW_USER_SRP_AUTH: Enable SRP-based authentication.
  * ALLOW_REFRESH_TOKEN_AUTH: Enables to refresh tokens.
* **AllowedOAuthFlowsUserPoolClient**: to be set to true if the client is allowed to follow the OAuth protocol
* Redirect, callback and logout URLs

Then we specify the scopes that the user pool client can provide. It defines what user data is accessed and provided in the ID Token:

* AllowedOAuthScopes: can be selected among:
  * email: get the user email attribute from the ID token,
  * phone: get the phone attribute only from the ID token,
  * profile: all attributes of the ID token can be accessed,
  * openid: required to get the ID token,
  * aws.cognito.signin.user.admin: provides access to  Amazon Cognito user pool API operations that require access tokens, such as UpdateUserAttributes and VerifyUserAttribute;

Additionnaly, custom scopes can be added to both the ID and access tokens, as we will see later in this article.

We can also specify the validity duration of every token types using AuthSessionValidity, AccessTokenValidity, IdTokenValidity, RefreshTokenValidity,TokenValidityUnits.

Finally, some additional parameters can be useful:

* **EnableTokenRevocation**: By default, tokens can be revoked using the Cognito API. With it, all the access tokens generated by the specified refresh token become invalid. ([see documentation](https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_RevokeToken.html))
* **PreventUserExistenceErrors**: this is a security setting to avoid leaking out information during authentication, account confirmation, and password recovery when the user does not exist in the user pool. It will return the same error in all cases and not UserNotFoundException.

For the listen application client this gives:

```yaml
AllowedOAuthFlows: 
- implicit
AllowedOAuthScopes: 
- email
- openid
AllowedOAuthFlowsUserPoolClient: true
CallbackURLs:
- "https://google.com" # Should be your app domain, google is convinient for our test
ClientName: !Sub ${AppName}-listen-app
ExplicitAuthFlows:
- ALLOW_USER_PASSWORD_AUTH # To be able to test the auth flow with CURL
- ALLOW_REFRESH_TOKEN_AUTH
PreventUserExistenceErrors: ENABLED
SupportedIdentityProviders:
- COGNITO 
UserPoolId: !Ref UserPool
```

## Authorization configuration




<!-- "Now, say for example, if a user only had access to your Android platform (android app client) and that app client had only read_product scope allowed in app client settings. Then the User accessing this Android platform (app client) can only get an access token which has the scope read_product allowed. Hence, thereby restricting their access to only the read access to the resource server. Where as you could have another platform (another app client under the same Cognito Userpool) which allows all scopes of the resource server.
Hence, to reiterate, scope is a level of access that an app can request to a resource. It all comes down to the developer to decide what platform (app client) has what scopes allowed (that one will get in access token), and which base of users get the access to which specific platforms (app clients) to exercise the granular restricted access."

Use cases patterns:
Users with different access levels access the apllication from the same UserPoolClient. (e.g. the android app allows access to both users and admins but grants different resource access). In such a case, you can use groups to add admins into the admin group. This will be provided as a custom claim in the access token (CognitoGroups:admin). You can set a specific lambda to put the group as a scope in the API. Removing some coupling between the auth server and the application. However,  -->


### Defining our OAuth scopes

Let's first see what users can do on our single "song" resource:

* Users and third-party applications can read songs
* Admins and creators can write songs

To support that, we define two custom resources

```yaml
Identifier: com.mydomain.song
Name: Song server
Scopes: 
- ScopeDescription: Read songs
ScopeName: read
- ScopeDescription: Create and delete songs
ScopeName: write
UserPoolId: !Ref UserPool
```

We must also add those scopes into relevant user pool clients:

```yaml
AllowedOAuthScopes: 
...
- song/read
- song/write
```

![demo](/posts/cognito_secure_api/api_scopes.jpg#medium)

### Defining our OAuth claims

On top of that, we have users that have additional privileges that depend on user attributes or roles:

* Premium users can read all songs
* Admins can write all songs

It would be convenient to retrieve this information directly from the token, as it is needed on all our endpoints. We can use specific claims to convey this information to the API, which will decide whether to authorize the request based on specific business rules.

Adding these custom claims to the token can be done:

* Either by triggering a lambda in the authentication flow. It would query the user database to retrieve this information and add the custom claims to the token;
* Or by using the Cognito groups feature. When a user belongs to a group, Cognito includes this information in the access token under the 'Cognito:group' claim. We could have admin and premium user groups.

The advantage of the first solution is that it is more flexible. Claims can have the names we want, and the database contains all the data needed to fill their value. The downside is that we trigger a lambda and query the database, at each login and refresh.

The advantages of the second solution are that no lambda trigger is needed, and we do not query the database. The downside is that we must comply with Cognito's way of doing it. It adds some coupling between the server authorization and the backend as group claims have a specific claim key. Finally, it requires synchronization between the database and Cognito for premium subscriptions.

For this example, we will go toward the simplest solution, which is using Cognito groups. We add two groups to our Cognito configuration:

* user_subscription_premium
* user_role_admin

Here is the syntax for the premium user group:

```yaml
Description: Premium users
GroupName: user_subscription_premium
UserPoolId: !Ref UserPool
```

## The full configuration file

The complete Cloudformation file describing our Cognito is too big to be displayed in this article. But you can consult it on [this repository](todo) on my Github account.

## Testing our configuration

There are two ways for getting a tokens from Cognito:
* either from the Cognito API using the [initAuth](https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_InitiateAuth.html) endpoint. But this endpoint does not comply with oauth 2.0. The access token retrieved does not include the custom scopes. And it seems it will not change any time soon, [see this issue](https://github.com/aws-amplify/aws-sdk-android/issues/684)
* or from the [hosted UI](https://docs.amazonaws.cn/en_us/cognito/latest/developerguide/cognito-userpools-server-contract-reference.html) which complies with OIDC and OAuth 2.0 protocols.

We'll then use the hosted UI for our tests. We set hosted UIs for each user pool following this model:

```yaml
Domain: my-unique-cognito-subdomain
UserPoolId: !Ref AdminPool
```

The UI can be customized to match your app style using the [UI Customization Attachment](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-cognito-userpooluicustomizationattachment.html) component.

### Bulding the sign in URL

To login, we target directly the [login endpoint](https://docs.amazonaws.cn/en_us/cognito/latest/developerguide/login-endpoint.html) as the [authorize endpoint](https://docs.amazonaws.cn/en_us/cognito/latest/developerguide/authorization-endpoint.html) is just a redirection there as we do not use an external IdP. 

We set following query string parameters:

* **client_id** is the id of the user pool client
* **redirect_uri** is the redirection url on succesful login, should be your domain
* **response_type** defines  the OAuth grant type we need. We chose token here so that we can visualize it directly without needing an additional call as with the code grant flow. However, beware that in this flow, no refersh token is returned as per OAuth 2.0 specification. See [here](https://aws.amazon.com/blogs/mobile/understanding-amazon-cognito-user-pool-oauth-2-0-grants/) a description of the code and token flows with Cognito.
* **scope** are the scopes asked by the client for the access token and also for defining wether an ID token is returned. If you ask for more scopes than the client can provide, you get a 400 HTTP error "invalid_scope".

These URLs are built as an output of the Clouformation stack.

### Sign up

When connecting to the user pool clients, I can register myself and my email is verified.  

![demo](/posts/cognito_secure_api/admin_signin.png#small)

While on the admin client, I can only sign in.

![demo](/posts/cognito_secure_api/signin_with_signup.png#small)

For the rest of the test, I signup using the hosted UI for the the UserPool. I receive an email with a confirmation code that I enter into the hosted UI for having my email set as verified.

Then I add my user to the adminPool using the AWS console. 

![demo](/posts/cognito_secure_api/user_create_console.png#medium)

I receive a temporary password on my email that I have to enter on first login. Then I am being requested to update my password and enter the defined user attributes.

![demo](/posts/cognito_secure_api/admin_first_signin.png#small)

Finally I scan a QR code with an authentication app and a trusted device so that I can login after with MFA:

![demo](/posts/cognito_secure_api/first_signin_mfa_setup.png#small)

### Sign in

To login, we target directly the [login endpoint](https://docs.amazonaws.cn/en_us/cognito/latest/developerguide/login-endpoint.html) as the [authorize endpoint](https://docs.amazonaws.cn/en_us/cognito/latest/developerguide/authorization-endpoint.html) is just a redirection there as we do not use an external IdP. 

We set following query string parameters:

* client_id is the id of the user pool client
* redirect_uri is the redirection url on succesful login, should be your domain
* response_type defines  the OAuth grant type we need. We chose token here so that we can visualize it directly without needing an additional call as with the code grant flow. However, beware that in this flow, no refersh token is returned as per OAuth 2.0 specification. See [here](https://aws.amazon.com/blogs/mobile/understanding-amazon-cognito-user-pool-oauth-2-0-grants/) a description of the code and token flows.
* scope are the scopes asked by the client for the access token and also for defining wether an ID token is returned. If you ask for more scopes than the client can provide, you get a 400 HTTP error "invalid_scope".

The sign in for the creator and listen app are different. There is no MFA for the user pool while the admin pool needs token MFA:

![demo](/posts/cognito_secure_api/mfa_request.png#small)

### ID tokens

You can consult the ID token claims on the [Amazon documentation](https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-the-id-token.html).

We focus here on the specific fields we are expecting, eluding standard claims:

player app Access token:

```json
{
  ...
  "email_verified": true,
  "cognito:username": "d6afc6ac-bdf6-49e1-b450-142bc1aa0e72",
  "email": "mange.thibault@gmail.com"
}
```

creator app ID token:

```json
{
  ...
  "email_verified": true,
  "cognito:username": "d6afc6ac-bdf6-49e1-b450-142bc1aa0e72",
  "email": "mange.thibault@gmail.com"
}
```

Admin ID token:

```json
{
  ...
  "email_verified": true,
  "cognito:username": "8ac0972b-d258-4d9d-bc6d-aa1fc9214950",
  "name": "titi",
  "family_name": "toto",
  "email": "mange.thibault@gmail.com"
}
```

### Access tokens

You can consult the Access token claims on the [Amazon documentation](https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-the-access-token.html).

We focus here on the specific fields we are expecting, eluding standard claims:

player app Access token:

```json
{
  ...
  "scope": "openid song/read email",
  "username": "d6afc6ac-bdf6-49e1-b450-142bc1aa0e72"
}
```

creator app access token:

```json
{
  ...
  "scope": "openid song/write song/read email",
  "username": "d6afc6ac-bdf6-49e1-b450-142bc1aa0e72"
}
```

Admin access token:

```json
{
  ...
  "scope": "openid song/write profile song/read",
  "username": "8ac0972b-d258-4d9d-bc6d-aa1fc9214950"
}
```

### Refresh tokens of the third-party client


## Conclusion

Your cognito skills are one level up! You know its capabilities and you're not afraid to use it. You'll be able to quickly get up to speed when needed. 

Finally, your users will benefit from having their access secured.

![Music app design](/posts/cognito_secure_api/main_illustration.jpeg#small)
