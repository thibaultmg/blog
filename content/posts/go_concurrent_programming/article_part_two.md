+++ 
draft = false
date = 2022-10-25T09:05:43+02:00
title = "Goroutines and channels, synchronization and communication — Part II"
description = "Working with Go's distinct approach of concurrency using goroutines and channels."
slug = "goroutines-and-channels-part-2"
authors = []
tags = ["Go", "concurrency", "goroutines", "channels"]
categories = []
externalLink = ""
series = ["Goroutines and channels"]
+++

In the first part, we introduced the two main Go concurrency primitives: goroutines and channels. After seeing how to control goroutines' lifetime and wait for their completion, we will focus more on how to use channels. Here are the topics we will cover:

* Limiting the concurrency level using semaphores and worker pools;
* Replacing standard concurrency primitives (mutex, semaphore, condition variable) behavior with channels;
* Sharing contextual data between goroutines;
* Distributing data with fan-in, fan-out, broadcast, and publish/subscribe patterns.

## Executing jobs in parallel

Goroutines define concurrent units of work; they allow executing jobs in parallel. However, our machines' processing capability is limited; we may need to limit the concurrency level for a given job. This can be achieved using a semaphore or workers pool.

### Using a semaphore {#using-a-semaphore}

A semaphore is a synchronization primitive that holds an internal counter and provides two operations: `wait` and `signal`. The counter is initialized with a positive value. `Wait` decrements and `signal` increments the counter. If the counter is zero, the semaphore is already fully shared, and `wait` blocks until the counter becomes greater than zero.

A buffered channel can mimic this behavior. The buffer size determines the number of tokens that can be used at the same time. `wait` is equivalent to writing a token into the channel, and `signal` is equivalent to reading a token from the channel. Such a semaphore allows limiting the number of concurrent goroutines. Here is an example:

```Go 
func main() {
    concurrencyLevel := 2
	semaphore := make(chan struct{}, concurrencyLevel)

	for i := 0; i < 3; i++ {
        semaphore <- struct{}{}

		fmt.Printf("start goroutine %d\n", i)
		go func(id int) {
            fmt.Printf("end goroutine %d\n", id)
			<-semaphore
		}(i)

	}

    // Wait end of goroutines
	for i := 0; i < concurrencyLevel; i++ {
        semaphore <- struct{}{}
	}

	fmt.Println("doing some more processing")
}
/*
Output:
start goroutine 0
start goroutine 1
end goroutine 1
start goroutine 2
end goroutine 2
end goroutine 0
doing some more processing
*/
```

Play with it on the [Go Playground](https://go.dev/play/p/eKFHrZ3Ljff).

### Using workers

Another common way of controlling the amount of concurrent work is to use workers. 

You spawn some workers and let them pull work from a common channel. This pattern is less common in Go because launching goroutines is very cheap compared to OS threads. Using a semaphore, you better manage your resources (no idle workers), and your code is simpler. However, for CPU-bound tasks, it may be more efficient to use workers.

Here is a possible implementation of the worker pool pattern in Go:

```Go
func main() {
	workersNb := 2
	jobsNb := 6

	jobResults := make(chan jobResult, workersNb)
	var wg sync.WaitGroup

	jobs := func() <-chan job {
		jobs := make(chan job, workersNb)
		go func() {
			defer close(jobs)
			for i := 0; i < jobsNb; i++ {
				jobs <- job{i}
			}
		}()
		return jobs
	}()

	for i := 0; i < workersNb; i++ {
		wg.Add(1)
		fmt.Printf("starting worker %d\n", i)
		go func(workerID int) {
			defer wg.Done()
			for j := range jobs {
				jobResults <- worker(j)
			}
			fmt.Printf("ending worker %d\n", workerID)
		}(i)
	}

	go func() {
		wg.Wait()
		close(jobResults)
	}()

	for res := range jobResults {
		fmt.Printf("finished job %d\n", res.ID)
	}
}

type job struct {
	ID int
}

type jobResult struct {
	ID int
}

func worker(j job) jobResult {
	return jobResult{j.ID}
}

/*
Output:
starting worker 0
starting worker 1
finished job 1
finished job 0
finished job 2
finished job 3
finished job 4
ending worker 0
finished job 5
ending worker 1
*/
```

Play with it on the [Go Playground](https://go.dev/play/p/M1qlUVqvYdU).

## Replicating standard concurrency primitives using channels

### Protecting a critical section

A critical section is a piece of code that must be executed by only one goroutine at a time. It is usually protected by a mutex. With Go, it can be protected using a buffered channel of size one.

```Go
type someWorker struct {
	m chan struct{}
}

func (w *someWorker) doWork() {
	w.m <- struct{}{}
	defer func() { <-w.m }()
	// Critical section
}
```

On my side, I usually prefer to use the mutex from the `sync` package instead of a channel. But it is a matter of taste.

### Protecting concurrent access to a shared resource

A shared resource access is usually protected by a mutex. With channels, the same result can be achieved using a buffered size one channel. The channel is used to communicate the shared resource to the goroutines that need to access it. Once the goroutine is done, it puts the resource back into the channel. If the resource is already being used, the goroutine will block until it is sent back to the channel.

```Go
type sharedResource struct {}

mutexChan := make(chan *sharedResource, 1)

func useSharedResource() {
	resource := <-mutexChan
	defer func() { mutexChan <- resource }()
	// Critical section
}
```

### Using a counting semaphore

As we have seen in the previous section, a counting semaphore can be implemented using a buffered channel. 

Please refer to the [previous section]({{< ref "#using-a-semaphore">}}) for an example.

### Condition variables

A condition variable is a synchronization primitive that allows a goroutine to wait for a condition to be met. The `sync` package in the standard library provides a condition variable implementation. But for most cases, you are better off using a channel. It is even stated in the [documentation](https://pkg.go.dev/sync#Cond) of the `sync` package:

> For many simple use cases, users will be better off using channels than a Cond (Broadcast corresponds to closing a channel, and Signal corresponds to sending on a channel).

Here is a simple implementation using a channel:

```Go
type condition struct {
	c chan struct{}
}

func (c *condition) wait() {
	<-c.c
}

func (c *condition) signal() {
	c.c <- struct{}{}
}

func (c *condition) broadcast() {
	close(c.c)
}
```

For more advanced examples, I highly recommend watching [this talk](https://www.youtube.com/watch?v=5zXAHh5tJqQ) by  Bryan C. Mills.

## Sharing contextual data between goroutines

The context package provides a way to share data between goroutines. It is a key/value store that can be passed as a parameter to any function that needs to access it. This pattern is not restricted to goroutines. More precisely, in the [documentation](https://pkg.go.dev/context#Context) of the context package, we can read:

> Use context Values only for request-scoped data that transits processes and APIs, not for passing optional parameters to functions.

A best practice is to encapsulate the key as an unexported type to avoid collision with other packages.
The key can be any comparable type. A named empty struct can do the job. Indeed, structs are comparable and are equal if:

* Their type is equal 
* And the value of their fields is the same

Thus, no other empty struct will collide with such a key. Here is an implementation example:

```Go
type userContextKey struct{}

type User struct {
    ID string
}

func NewContextWithUser(ctx context.Context, val *User) context.Context {
    return context.WithValue(ctx, userContextKey{}, val)
}

func UserFromContext(ctx context.Context) (*User, bool) {
    ret, ok := ctx.Value(userContextKey{}).(*User)
	return ret, ok
}
```

This pattern can, for example, be used on a web server. An authentication middleware can inject the user in the context so that any operation done afterward by other functions processing the request can have access to it.

## Sharing data between goroutines

This section focuses on using channels to distribute data for pipeline processing and publish/subscribe patterns. We'll see how to implement the following ones:

* **fan-out**: data is consumed by multiple goroutines, and each data piece is consumed by a single goroutine. This mode is natively supported by channels. Each goroutine loops over the same common channel to read its values;
* **fan-in**: data from multiple channels are combined into a single one;
* **broadcast**: data is distributed to all consumers, each consumer getting a copy of the data;
* **publish/subscribe**: data is distributed to a subset of consumers. Each consumer gets a copy of the data.

### Broadcast

Broadcasting the same value once to each consumer cannot be done with a single output channel in which we duplicate the data. Because as goroutines loop over the channel to read its values, some may consume the value several times, and other goroutines will get none.

Broadcasting requires having multiple distribution channels, one per consumer. Then, we use two nested loops, where the outer one consumes the input channel, and the inner one broadcasts values to output channels. Here is an implementation example:

```Go
func broadcast(ctx context.Context, in <-chan int, outList []chan int) {
    for {
        var inVal int

		select {
            case <-ctx.Done():
			return
		case v, ok := <-in:
			if !ok {
                return
			}
			inVal = v
		}

		for _, out := range outList {
            select {
                case <-ctx.Done():
				return
			default:
				out <- inVal
			}
		}
	}
}
```

Play with it on the [Go Playground](https://go.dev/play/p/ortifuLsWls)

### Combine multiple channels into one — fan-in

Combining multiple channels into a single channel cannot be achieved using a single loop consuming the input channels. If one of the input channels is empty, the loop will block until a value is available on that channel. And we stop consuming the other channels. 

One solution requires using a goroutine for each input channel. When it reads a value, it pushes it into the single output channel. Here is an implementation example:

```Go
func fanin(ctx context.Context, in []<-chan int) <-chan int {
	ret := make(chan int)
	var wg sync.WaitGroup

	wg.Add(len(in))

	for _, c := range in {
		go func(inChan <-chan int) {
			defer wg.Done()
			for {
				select {
				case <-ctx.Done():
					return
				case val, ok := <-inChan:
					if !ok {
						return
					}
					ret <- val
				}
			}

		}(c)
	}

	go func() {
		wg.Wait()
		close(ret)
	}()

	return ret
}
```
Play with it on the [Go Playground](https://go.dev/play/p/ono7S4befJu)

### Publish/subscribe 

Publish/subscribe is a pattern where a publisher sends a message to a channel, and subscribers receive it. This pattern is often used to notify subscribers of an event. This is notably the case for the [signal.Notify](https://pkg.go.dev/os/signal#Notify) function.

Channels can be used to implement this pattern. The subscriber provides the channel it wants to receive the event on. The publisher sends the event to the channels that have subscribed to the event. 

Here is an implementation example based on the `signal.Notify` function:


```Go
type EventType uint

const (
	StepA EventType = 1 + iota
	StepB
)

func (e EventType) String() string {
	switch e {
	case StepA:
		return "stepA"
	case StepB:
		return "stepB"
	default:
		return ""
	}
}

type Event struct {
	// m is a chan (buffered of size 1) that acts as a synchronization mecanism for acessing the map.
	// The map holds the channels of subscribers as keys and the subscribed events as values.
	m chan map[chan EventType]uint
}

// NewEvent returns an Event object
func NewEvent() *Event {
	m := make(chan map[chan EventType]uint, 1)
	m <- make(map[chan EventType]uint)
	return &Event{m}
}

// Subscribe allows channel c to subscribe for event of type eventType.
func (e *Event) Subscribe(c chan EventType, eventType EventType) {
	subscribers := <-e.m
	subscribers[c] |= 1 << eventType
	e.m <- subscribers
}

// Notify sends an event of type eventType to all subscribers.
func (e *Event) Notify(eventType EventType) {
	subscribers := <-e.m
	for c, subs := range subscribers {
		if subs&(1<<eventType) != 0 {
			c <- eventType
		}
	}
	e.m <- subscribers
}
```

Play with it on the [Go Playground](https://go.dev/play/p/_uZoMzor8HT).

## Conclusion

Congratulations! You've reached the end of this long article. It covers many details on how to use channels and goroutines in Go. These primitives are distinct from other programming languages and bring new ways of thinking about concurrency. It promotes sharing data between goroutines by communicating through channels instead of synchronizing access to shared data through external synchronization mechanisms such as mutexes.

Finally, remember the Go mantra from Rob Pike:

> Do not communicate by sharing memory; instead, share memory by communicating.

You probably now have a deeper understanding of it.

Thanks for reading! For writing this article, I used the following resources:

* Great presentation from Bryan C. Mills on how to use channels instead of classic synchronization primitives: [youtube](https://www.youtube.com/watch?v=5zXAHh5tJqQ), [slides](https://drive.google.com/file/d/1nPdvhB0PutEJzdCq5ms6UI58dp50fcAN/view)
* The channels [go memory model](https://go.dev/ref/mem#chan)
* Effective Go: [Channels](https://golang.org/doc/effective_go#channels)













