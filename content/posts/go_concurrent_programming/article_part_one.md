+++ 
draft = false
date = 2022-10-17T09:05:43+02:00
title = "Goroutines and channels, synchronization and communication — Part I"
description = "Working with Go's distinct approach of concurrency using goroutines and channels."
slug = "goroutines-and-channels-part-1"
authors = []
tags = ["Go", "concurrency", "goroutines", "channels"]
categories = []
externalLink = ""
series = ["Goroutines and channels"]
+++

One distinct feature of the Go programming language is its approach to concurrent programming. You don't rely directly on OS-provided threads and don't share variables between concurrent units of work. You have to think differently from what you are used to. You must share and synchronize by communicating. To support that, Go introduces two unique primitives that we will study in this article.

Nevertheless, you can still share variables and synchronize their access through traditional mutexes or atomic operations. And it might be more straightforward and more readable in some cases. More often, though, you're better off doing the same thing by communicating.

This article provides some tooling and patterns for working with goroutines and channels, leaving performance aside. The content is split into two parts. Here is what we will cover in this first part:

* The goroutine and channel primitives details;
* Controlling the goroutine lifetime with the context object;
* Waiting for goroutines to finish their work using waitGroups and errorGroups.

[Part II]({{< ref "article_part_two" >}}) will be about the multiple usages of channels.

## Go concurrency primitives

Go introduces two primitives in its language to support concurrency and communication between concurrent units of work:

* `go`: starts a concurrent unit of work called a goroutine;
* `chan`: is a communication mean between goroutines. Like a pipeline in which you can stream data.

As you may know, concurrency is not parallelism. Concurrency is a compile-time property. You specify that pieces of code can run independently and potentially at the same time. Parallelism is a runtime property. It means your concurrent code is being run in parallel on several CPU cores. This is why we use the word "concurrency" here.

### The goroutine

The `go` primitive is straightforward to use. You put it in front of a function call containing your concurrent piece of work:

```Go
go myFunction() // Create a goroutine for executing the function, and continue without waiting.
```

This function execution is then handled by the Go runtime that will schedule it on one (or many over its execution time) of the OS threads it manages. You have no control over the goroutine itself. Don't worry about it, but keep in mind that you have no guarantee of when it will be executed. As specified in the [memory model](https://go.dev/ref/mem#goexit): 

* The exit of a goroutine is not guaranteed to be synchronized before any event in the program.

We will explore later how to synchronize with it. But remember that everything run by the go runtime is a goroutine, even the main function.

Another thing that is unique about goroutines is that they are very lightweight. The allocated stack size is about 2Kb and is growable as opposed to the OS threads that are much more costly. Spawning a goroutine is almost a no-cost operation. This is an important consideration because it enables different programming patterns. Go programs can run tens of thousands of goroutines without any problem.  

### The channel

The `chan` primitive is more complicated to use because there are a few rules to be aware of. 

Let's start with what it is. It is a data structure that must be initialized to be used with `make` (as for a slice or a map). It returns a reference to the data structure created by `make`. Then you can pass them directly as function arguments without handling pointers yourself. The argument will point to the same channel in the receiving function. And they are naturally safe to use concurrently.

A channel is defined by two properties:

* A type defining the objects it can communicate
* And a buffer size defining the number of objects it can hold. 

They are comparable and equal if they point toward the same underlying data structure. You can thus use them as keys in maps, for example. Here is the base syntax for using channels:

```Go
chA := make(chan int) // Unbuffered channel of type int.
chB := make(chan int, 1) // Buffered channel of type int and size 1.

close(chA) // Closes the channel. Its readers will receive default values
len(chB) // As for slices, returns the number of items in the buffered channel
cap(chB) // As for slices, returns the capacity of the buffered channel

chB<-1 // Writes the value 1 in the channel.
res:= <-chB // Read the channel value and assign it to a variable.
res, ok:= <-chB // ok variable if false if the channel is closed

for val := range chA // Reads the channel until it closes

select { // Reads several channels at the same time. It multiplexes read.
    case val := <-chA:
        doSomething(val)
    case <-chB:
        doSomethingElse()
}

func readChan(c <-chan int) {} // In function signatures, you can specify read or write restrictions. It is used at compile time by the type system.
```

A channel state can be nil, allocated, or closed. The specificity of the closed channel, which is not nil, is that it will return the default value of its type indefinitely. 

As a side note, you only need to close a channel to preserve memory if the receiving goroutines need to know about it. Once determined as unreachable by the garbage collector, its resources will be reclaimed whether it is used or not.

Depending on the state and type of the channel and the operation being proceeded, the code will either not compile, proceed, block, or panic. Here is the table describing the rules:

| Channel state | Read | Write | Close |
| ------------- | ---- | ----- | ----- |
| nil | 💤 Block | 💤 Block | 💥 Panic |
| Contains a value, is not full | ✅ Execute | ✅ Execute | ✅ Execute |
| Contains a value, is full | ✅ Execute | 💤 Block | ✅ Execute |
| Closed | ✅ Execute | 💥 Panic | 💥 Panic |
| Read only | ✅ Execute | 🛑 Compilation error  | 🛑 Compilation error |
| Write only | 🛑 Compilation error | ✅ Execute | ✅ Execute |

Regarding their operations order guarantees, they are defined in the [Go memory model](https://go.dev/ref/mem#chan):

* A send on a buffered channel is synchronized before completing the corresponding receive from that channel.
* A receive from an unbuffered channel is synchronized before completing the corresponding send on that channel.
* The kth receive on a channel with capacity C is synchronized before completing the k+Cth send from that channel completes.
* The closing of a channel is synchronized before a receive that returns a zero value because the channel is closed.

Based on the first guarantee, we get "ping pong" from the following code:

```Go
var c = make(chan int)

func f() {
	fmt.Println("ping")
	<-c // Is guaranted to complete before the write with unbuffered channel
}

func main() {
	go f()
	c <- 0 // Is guaranted to complete after the read with unbuffered channel
	fmt.Println("pong")
}
// Output:
// ping
// pong
```

While the same code with a buffered channel offers different guarantees. The write is guaranteed to happen before the read. But what happens after can be anything between:

* the program closes before the goroutine execution, resulting in only "pong";
* the goroutine is executed before the "pong", resulting in "ping pong";
* the goroutine is executed after the "pong" and before the program exits, resulting in "pong ping".

```Go
var c = make(chan int, 1)

func f() {
	fmt.Println("ping")
	<-c // We have no guarantee for its execution.
}

func main() {
	go f()
	c <- 0 // Is guaranteed to happen before the read for a buffered channel.
	fmt.Println("pong")
}
```
See [the go playground](https://go.dev/play/p/s5yFX2VsA85)

### Clean code

Goroutines and channels are handy, but the code can get messy if you don't know what is synchronous. To keep your code readable, you should try to follow a few simple rules:

* Package interfaces should expose synchronous functions and let the caller decide if your unit of work can be run concurrently
* Keep the channels local to the calling function when possible
* Follow clear ownership rules for channels. The owner of a channel is the creator, the writer, and the closer. The consumer only reads from it.

We will apply these principles in the code examples below when applicable.

## Controlling a goroutine lifetime

### Stopping a goroutine

Goroutines are independent units of work over which you have no control. This can be problematic when your goroutine does a long-running task, a repetitive job, or a request over a network that is not needed anymore. To cancel their work, you must send them a message telling them to stop. The idiomatic way of doing it is by using the context lib. This package is a relatively recent addition to the standard lib, dating from Go v1.17. 

From the [documentation](https://pkg.go.dev/context@latest) of the context package, we see:

> Package context defines the Context type, which carries deadlines, cancellation signals, and other request-scoped values across API boundaries and between processes.

A Context is an immutable data structure from which new contexts can be derived, keeping the parent properties and adding new ones. It is safe to use concurrently. Also, you should never use a nil context. Conventionally, nil value is never checked and would result in panics when used in called functions. Always initialize it with `TODO` or `Background`.

On the calling side, to make a goroutine cancellable, we derive a context with cancel capabilities from a base context:

```go
// The returned cancel function can be used to tell the consuming functions to abandon their work. 
// Once canceled, successive calls do nothing
ctx, cancel := context.WithCancel(context.Background())
```

You have no guarantee by the language that the cancel function will apply. You rely on the called function to implement proper handling of the context.

On the receiving side, you listen to the Done channel. This channel is closed by the context when it is canceled. Once closed, you can retrieve the error type (Canceled or DeadlineExceeded) with `ctx.Err()`. Here is an example of how it can be implemented where the done channel is checked before processing each job:

```Go
for _, job := range jobs {
    select {
    case <-ctx.Done():
        return nil, ctx.Err()
    default:
    }

    // do some work
} 
```

Here is another implementation example for a long-running operation where resources can be released early:

```Go
func doWithContext(ctx context.Context, req Request) (Result, error) {
    result := make(chan Result)
    go func() {
        result<- longRunningWork()
    }

    select {
        case <-result:
            return result, nil
        case <- ctx.Done():
            releaseResources()
            return nil, ctx.Err()
    }
}
```

This is done with the `CommandContext` (from the `os/exec` package) function, where the provided context is used to kill the process (by calling os.Process.Kill) if the context becomes done before the command completes on its own. See implementation [here](https://cs.opensource.google/go/go/+/refs/tags/go1.19.2:src/os/exec/exec.go;l=644;drc=6b22572e700235fb7303c7fd6aefcc33c743a130).

In other instances, the function returns early, but side effects cannot be prevented, as with network calls, for example.

### Setting timeouts and deadlines

The context can also be used to set deadlines and timeouts:

```Go
// At a cetain point in time.
// If the date is sooner than the parent, the new context is equivalent to the parent.
ctx, cancel := context.WithDeadline(context.Background(), date)

// After a certain duration from now.
// Is equivalent to WithDeadline(parent, time.Now().Add(timeout))).
// A longer time than the parent will do nothing; just reuse the parent deadline.
ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
```

When the timeout expires or the deadline is reached, the context is automatically canceled. The context is used exactly as described previously in the receiving function.

## Waiting for goroutines to finish their work

### Wait from concurrent asynchronous calls

We have two synchronous functions that we can make concurrently and wait for the results. To achieve that, we assign their results to local channels from the goroutines using closures. We then read their value after launching the goroutines. The read operation blocks until we get the result:

```Go
func main() {
	resultA := make(chan string)
	resultB := make(chan string)

	go func() {
		resultA <- computeSomething()
	}()

	go func() {
		resultB <- computeSomething()
	}()

    //... doing some more work

	// Waiting for the results of the async calls
	computedStringA := <-resultA
	computedStringB := <-resultB
}

func computeSomething() string {
	return "hello"
}
```

### Wait for many goroutines

The same result can be achieved using the `WaitGroup` data structure from the `sync` package. It is a thread-safe counter that can be incremented with `Add` and decremented with `Done`. The `Wait` function blocks until the counter is 0. Here is an example of how it can be used:

```Go
func main() {
	var resultA, resultB string
	var wg sync.WaitGroup

	wg.Add(2) // Must be added synchronously, before the wait call

	go func() {
		defer wg.Done()
		resultA = computeSomething("A")
	}()

	go func() {
		defer wg.Done()
		resultB = computeSomething("B")
	}()

	//... doing some more work

	// Waiting for the results of the async calls
	wg.Wait()
	fmt.Println(resultA, resultB)
}

func computeSomething(s string) string {
	return s
}
```

see [Go playground](https://go.dev/play/p/Ag1wVpVkVvz)

### Wait for many goroutines and return early if one fails

ErrGroup is similar to the WaitGroup described previously. But allows returning early if one of the goroutines returns an error. This is achieved by adding error propagation and context management. 

From the documentation, we read how to use its main functions:

* `WithContext`: The derived Context is canceled the first time a function passed to Go returns a non-nil error or the first time Wait returns, whichever occurs first.
* `Wait`: Blocks until all function calls from the Go method have returned, then returns the first non-nil error (if any) from them.
* `Go`: The first call to return a non-nil error cancels the group's context if the group was created by calling WithContext.

Here is a code example with an errorGroup returning early as a function returns an error. The context gets canceled, and the longer subtask is not executed:

```Go
func main() {
	eg, ctx := errgroup.WithContext(context.Background())
	var res1, res2 string

	eg.Go(func() error {
		var err error
		res1, err = SubTask(ctx, 1, 1*time.Second)
		return err
	})

	eg.Go(func() error {
		var err error
		res2, err = SubTask(ctx, 2, 2*time.Second)
		return err
	})

	if err := eg.Wait(); err != nil {
		fmt.Printf("error: %v\n", err)
		return
	}

	fmt.Println(res1)
	fmt.Println(res2)
}

func SubTask(ctx context.Context, id int, d time.Duration) (string, error) {
	select {
	case <-ctx.Done():
		return "", fmt.Errorf("subtask context is done: %w", ctx.Err())
	case <-time.After(d):
		if id == 2 {
			return "", fmt.Errorf("subtask forbidden id %d", id)
		}
	}

	return fmt.Sprintf("subtask %d is done.", id), nil
}

/*
Output:
error: subtask forbidden id 2
*/
```

Play with it on the [Go Playground](https://go.dev/play/p/PwT2J_1U7kJ).


## Coffee break

This is a perfect time to take a break and drink a coffee. You have learned a lot about concurrency in Go. Controlling the goroutines lifetime and waiting for them to finish has no more secrets for you.

When you come back, we will focus more on controlling the concurrency level and how to use channels. See you then!

