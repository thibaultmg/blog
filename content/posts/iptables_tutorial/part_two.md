+++
draft = false
date = 2022-01-03T22:06:20+01:00
title = "Regain control over your network with Iptables — Extensions — Part II"
description = "Second part of the Iptables tutorial series. Use extensions and build your own chains."
slug = "iptables-tutorial-part-two"
authors = []
tags = ["Linux", "Iptables", "network", "firewall", "tutorial"]
categories = []
externalLink = ""
series = ["Iptables tutorial"]
hidden = false
+++

The present post is the second part of a series aiming at helping you regain control over your network using Iptables. The first part's purpose was to familiarize you with Iptables principles and build an actionable mental model to apply to our use cases. We mainly focused on the filter table and in Input chain. Now, we'll see more advanced usage with extensions and user chains.

![Netfilter tables](/posts/iptables_tutorial/netfilter_tables.jpg#medium)

For reference, the series' articles list is at the [bottom]({{< ref "#see-also-in-iptables-tutorial" >}}) of the page.

## More advanced use cases using extensions

Iptables have many extensions that will help you implement more complex use cases. The usage syntax is the following (you can consult with `man iptables-extensions`):

```bash
iptables [-m name [module-options...]]  [-j target-name [target-options...]
```

As we can see, there are two types of extensions: __match__ (`-m') and __target__ (`-j`). Additionally, other extensions are automatically made available based on the protocol used with `-p` (tcp, udp, or icmp).

## Match type extensions

In the first part of this tutorial, we saw that standard matchers enable rules to select addresses, protocols, ports, or network interfaces. Match extensions provide additional, more complex ways to capture packets. Enabling, for example, the usage of states to match packets. They are activated with the `-m' option. Then, depending on the extension, various additional command line options are made available. Below are a few extensions you might find helpful (there are around 60 match extensions in total).

Here are the extensions described here, depending on the property being matched:

* The linux user — [Owner]({{< ref "#owner" >}})
* The time of the day — [Time]({{< ref "#time" >}})
* The number of parallel connections — [Connlimit]({{< ref "#connlimit" >}})
* The number of packets or data by unit of time — [Hashlimit]({{< ref "#hashlimit">}}), [Recent]({{< ref "#recent">}})
* The state of a connection — [Conntrack]({{< ref "#conntrack" >}})

Some others add side effects like [Comment]({{< ref "#comment">}}) that adds a comment to the rule.

There are many more interesting extensions that might fit your needs. For example, you can match with cgroup membership with `cgroup`, or match packets based on the number of bytes or packets a connection has transferred so far with `connbytes`. You can find the complete list of extensions with `man iptables-extensions`.

### Comment extension

Comment allows you to add comments to any rule. These comments are then displayed when listing rules, for example.

```bash
$ iptables -A INPUT -i eth1 -m comment --comment "my local LAN" -j ACCEPT

$ iptables -L INPUT
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
ACCEPT     all  --  anywhere             anywhere             /* my local LAN */
```

### Connlimit extension

Connlimit restricts the number of parallel connections to a server per client IP address (or client address block).

To limit the number of parallel HTTP requests to 16 per 24 bit netmask CIDR blocks:

```bash
$ iptables -p tcp --syn --dport 80 -m connlimit --connlimit-above 16 --connlimit-mask 24 -j REJECT
# --syn is an extension enabled by the tcp protocol. It matches packets used to initiate a new connection.
```

### Conntrack extension

Conntrack allows access to the connection tracking state for this packet/connection. The primary use case is to match packets according to the state of the connection. For example, you can forbid connection to be initiated from the outside. But allow packets to be sent back to the outside if the connection was initiated from the inside.

 Reading the `man iptables-extensions` documentation, the main states are:

* **NEW**: The packet has started a new connection.
* **ESTABLISHED**: The packet is associated with a connection that has seen packets in both directions.
* **RELATED**: The packet is starting a new connection but is associated with an existing connection, such as an FTP data transfer or an ICMP error.

To allow established connections to remain open:

```bash
$ iptables -A INPUT -m conntrack --cstate ESTABLISHED,RELATED -j ACCEPT
```

### Hashlimit extension

Hashlimit enables rate limiting on the match using hash buckets (a particular data structure). More simply said, you can rate limit using one or a combination of `{srcip,srcport,dstip,dstport}`. The chosen combination defines the hash bucket. All packets that match the same bucket will be rate limited according to the specified rule. The hash combination can, for example, be defined as:

* `srcip`: rate limit per source IP address;
* `srcip,dstip,srcport,dstport`: rate limit per connection (source IP address, destination IP address, source port, destination port).

A rule defines the maximum number of packets or data by a unit of time. It also allows the definition of a burst size.  

Hashlimit provides about the same functionalities as the __recent__ extension. If you don't use the `--hashlimit-mode`, it acts like the __limit__ extension.

```bash
# limit to 1000 packets per second for every host
$ iptables -A INPUT -m hashlimit --hashlimit-mode  srcip  --hashlimit-upto 1000/sec -j ACCEPT

# reject flows exceeding 512kbyte/s
$ iptables -A INPUT -m hashlimit --hashlimit-mode srcip,dstip,srcport,dstport --hashlimit-above 512kb/s
```

### Limit extension

Limit will match until this limit is reached. It also includes burst capability with `--limit-burst`, which is 5 by default. It can be combined with the LOG and TRACE targets (that we cover below) to give limited logging, for example.

```bash
$ iptables -A INPUT -s 192.168.1.0/24 -m limit --limit 10/sec -j LOG
```

### Owner extension

Owner attempts to match various characteristics of the packet creator (user and group) for a locally generated packet. It applies to the OUTPUT table.

To forbid a user from connecting to a server on port 5000:

```bash
$ iptables -A OUTPUT -p tcp --dport 5000 --match owner --uid-owner 1000 -j DROP
```

### Recent extension

Recent allows you to dynamically create a list of IP addresses and then match against that list in a few different ways. Ok, this sentence is abstract; let's clarify. 

You create a named list with `--name` in which you save the packet's source address with `--set`. You then update the last seen timestamp in another rule wit `--update`. Then you can use matchers for IPs in the list with `--seconds` to see if the IP appeared in the last x seconds and refine it with `--hitcount` to specify a min number of hits. The following rules limit the number of new connections on port 22 to 3 by minute for a given IP to protect against brute force attacks:

```bash
# Create a new list named BRUTEFORCE
$ iptables -A INPUT -p tcp --dport 22 \
       -m conntrack --ctstate NEW \
       -m recent --set --name BRUTEFORCE \
       -j ACCEPT

# Reject if the IP is in the list and appeared in the last 60 seconds 3 times
$ iptables -A INPUT -p tcp --dport 22 \
       -m conntrack --ctstate NEW \
       -m recent --update --seconds 60 --hitcount 3 --name BRUTEFORCE \
       -j DROP
```

### Time extension

Time matches if the packet arrival date and time are within a given range. All times are interpreted as UTC by default. You can define start & end times, day of the week, and day of the month. To reject connections during the night, we would write:

```bash
$ iptables -A INPUT -m time --timestart 23:00 --timestop 6:00 -j REJECT
```

## Using target type extensions for logging

Here are a few targets that you might find helpful for logging. 

**AUDIT** target creates audit records for packets matching the associated rule. It works with the Linux kernel audit framework. 

**LOG** target uses kernel logging for packets matching the associated rule. You can read them with `journalctl -k`. Here is an example of a rule that logs all TCP packets:

```bash
iptables -A INPUT -p tcp -j LOG --log-prefix "tcp packet" --log-level debug
```

**TRACE** is a very handy target that we can use with `xtables-monitor` or `nft monitor trace` to debug our rules, as we will see in the third part of this tutorial series.

## Grouping rules with user-defined chains

As we have seen, extensions open up many new possibilities. Your rules will add up and become more complicated to organize. This is where user chains can be beneficial.

![Netfilter tables](/posts/iptables_tutorial/user_chain.jpg#small)

To illustrate such a case, we will create our own chain to rate limit ssh connections and log when the limit is exceeded. This use case is more complex with several rules; we will group them in our own chain.


To manage user-defined chains, here are the Iptables command flags:

```bash
-N chainName        # Create a new chain
-X chainName        # Delete the chain
-E oldName newName  # Update the chain name
```

Let's first create our `SSHCHECK` table that will handle the rate-limiting and `LOGDROP` that will log attempts exceeding our rate limit:

```bash
iptables -N SSHCHECK
iptables -N LOGDROPSSH
```

We then forward all packets using tcp with destination port 22 to our `SSHCHECK` chain.

```bash
iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW -j SSHCHECK
```

Within `SSHCHECK`, we track source IPs trying to create new connections too often and rate limit to 3 attempts per minute. If it exceeds, we send packets to our `LOGDROPSSH` chain:

```bash
iptables -A SSHCHECK -m recent --name sshcheck --update --seconds 60 --hitcount 3 -j LOGDROPSSH
iptables -I SSHCHECK -m recent --name sshcheck --set -j ACCEPT
```

Finally, let's configure our `LOGDROPSSH` chain that will log the attempt and drop the packet:

```bash
iptables -A LOGDROPSSH -j LOG --log-prefix "iptables rate limit ssh: " --log-level warning
iptables -A LOGDROPSSH -j DROP
```

## Editing packets with the NAT table

The NAT table is used to apply Network Address Translation (NAT) rules on network packets. To do that, it edits the headers of IP (for source and destination IPs) and TCP packets (for source and destination ports). It also stores the mapping of the old ip/port with the new ip/port to revert the operation for packets returning.

So, NAT allows several hosts to share the same IP address. It can be done in two flavors:

* __SNAT__ translates the __source__ address & port of the packet.
This is mainly used to share a public IP address between several hosts in a private network for accessing the internet. The machine holding the public IP will translate internal IPs of network packets with its public one before going to the internet. In such a case, the rule is applied in the postrouting chain to include forwarded packets.
* __DNAT__ translates the __destination__ address & port of the packet.
It can hide several servers behind a unique public IP address. In such a case, the rule is applied in the prerouting chain to include forwarded packets.

Finally, as the Nat table can edit the packet ports, it can also redirect our local traffic through a local proxy with the REDIRECT target.

Let's see the nat table into action.

### Creating a NAT Gateway — SNAT

As stated previously, the typical use case for the SNAT target is to create a gateway to access the internet from a local network without having a public IP. To configure your gateway, using its public IP 1.2.3.4, add:

```bash
$ iptables -t nat -A POSTROUTING -o eth1 -j SNAT --to-source 1.2.3.3
```

With `-o` being your network interface connected to the internet and `--to-source` the public IP of the NAT gateway.

For the gateway to work correctly, remember to enable IP forwarding. It can be done permanently by adding the following line to _/etc/sysctl.conf_:

```bash
net.ipv4.ip_forward = 1
```

Finally, clients on the local network must be configured to use the private IP of the NAT gateway as their own default gateway.

For cases where the public IP of the gateway is dynamically assigned, you can use the MASQUERADE target instead of SNAT. It will use the current source IP of the packet to translate it. Here is an example:

```bash
$ iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
```

This mode requires more resources than SNAT. So, if you don't need it, do not use it.

### Load balancing requests to a web server — DNAT

To illustrate the usage of the DNAT target, we will configure a load balancer. DNAT will change the destination IP/port of incoming packets. To load balance across several IPs, we will use the `statistic` module that will allow us to match packets with a given probability, and the remaining packets will be matched by the last rule.

```bash
# Half of the connections will match this rule and be redirected to IP 10.0.0.10 on port 8080
$ iptables -A PREROUTING -t nat -p tcp -d 1.2.3.4 --dport 80 \
         -m statistic --mode random --probability 0.5              \
         -j DNAT --to-destination 10.0.0.10:8080

# The other half that has not matched the previous rule will be redirected to IP 10.0.0.11 on port 8080
$ iptables -A PREROUTING -t nat -p tcp -d 1.2.3.4 --dport 80 \
         -j DNAT --to-destination 10.0.0.11:8080
```

### Redirecting traffic to a local proxy

Let's say we want to redirect all incoming traffic on port 80 to a local proxy running on port 8080. We can do that with the REDIRECT target:

```bash
$ iptables -A PREROUTING -t nat -p tcp --dport 80 -j REDIRECT --to-port 8080
$ iptables -A INPUT -p tcp --dport 8080 -j ACCEPT
```

## The remaining tables

Now that we have covered the usage of the filter and nat tables, there remain the last three tables that are raw, mangle, and security. These tables that aim to solve use cases that are more restricted and specific are grouped in this part. As a reminder, here are their position in the Iptables chains:

![Netfilter mange and security tables](/posts/iptables_tutorial/netfilter_mangle_sec.jpg#medium)

### Raw

The __raw__ table is the first to process incoming and outgoing packets. It only exists in the PREROUTING and OUTPUT chains. As we'll see in the following article, it helps with setting tracing. It will trigger logs for every rule matching the packets as those traverse the tables and chains.

Another use case is to handle connection tracking. As it operates before any connection tracking occurs, it can disable it. This allows saving some resources on your server. This can be done with the following command:

```bash
$ iptables -A INPUT -t raw -p tcp --dport 443 -j NOTRACK
```

### Mangle

The __mangle__ table appears first in every chain, except when raw is present. It allows modifying complementary fields of the IP and TCP packets headers compared with what nat can do and applying kernel markers. It handles a set of specific targets, such as:

* TTL can be used to change the Time To Live value of the packet. This is an 8 bits value that is decremented by each packet's router. When it reaches 0, the packet is discarded.
* MARK is used to set a mark value on the packet. Other modules can use this value to apply specific rules on the packet.
* TEE duplicates the packet and sends it to a specific IP address. This is useful for logging purposes.

### Security

The __security__ table follows the filter table in the chains and is specifically used to set security-related marks on packets at the kernel level using specific targets. Those marks can then be used by security subsystems such as SELinux. It is used for applying MAC (Mandatory Access Control) policies.

## Conclusion

We now have a broader view of Iptables capabilities. But with all these tables and chains, it can take time to understand what is going on. In the next article, we will see how to use Iptables to trace packets and help you debug your rules. Stay tuned!
