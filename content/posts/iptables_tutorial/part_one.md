+++
draft = false
date = 2021-12-21T18:09:05+01:00
title = "Regain control over your network with Iptables — Basics — Part I"
description = "First part of the Iptables tutorial series. Lay the foundations and build your first rule."
slug = "iptables-tutorial-part-one"
authors = []
tags = ["Linux", "Iptables", "network", "firewall", "tutorial"]
categories = []
externalLink = ""
series = ["Iptables tutorial"]
+++

Are you confident that your Linux machine's external access is well protected? You can quickly answer this question if you know how to use __Iptables__. While this tool seems complicated at first sight, its usage is not restricted to seasoned system administrators. Once you understand a few basic principles of Iptables inner workings, you will be able to use it to its full potential. This three parts guide will help you to regain control over your network.

![demo](/posts/iptables_tutorial/iptables_meme.jpg#small)

For reference, the series' articles list is at the [bottom]({{< ref "#see-also-in-iptables-tutorial" >}}) of the page.

## Foundations

### Capturing network packets

To manipulate network packets in Linux, we use __Netfilter__. It is a Linux networking kernel module having access to network packets traversing the system. It provides valuable functionalities to handle those packets.

To access and control Netfilter, we use a user-space program, __Iptables__. We could also have used the uncomplicated firewall (ufw).

![TCP/IP layers](/posts/iptables_tutorial/netfilter_iptable.jpg#400)

Together, Netfilter and Iptables allow filtering and altering IPv4 network packets. They operate at the network and transport layers of the TCP/IP model.

![TCP/IP layers](/posts/iptables_tutorial/transport_layers.jpg#small)

### Netfilter

Netfilter provides hooks triggered by every packet traversing the kernel networking stack. Hooks can register callback functions to apply filtering and modification rules on network packets.

Netfilter provides five hooks:

* __Prerouting__: when the packet arrives at the network interface from the outside world
* __Input__: when the packet arrives at the local process
* __Output__: when the packet leaves the local process
* __Postrouting__: when the packet arrives at the network interface from the local system
* __Forward__: captures packets not destined to the local system (for example, when acting as a router).

The following diagram illustrates the hooks organization.

![Netfilter's hooks](/posts/iptables_tutorial/netfilter_hooks.jpg#medium)

Packets, depending on the use case, can follow three possible paths:

1. From the network interface to the local process;
2. From the network interface to the network interface;
3. From the local process to the network interface.

![Netfilter possible paths](/posts/iptables_tutorial/netfilter_paths.jpg#medium)

### Iptables

As stated earlier, Iptables is the user-space program that controls Netfilters.
Iptables refers to Netfilter's hooks as __chains__. And for each chain, Iptables can apply different types of functions, referred to as __tables__.
Each rule stored in a table is evaluated one after the other, in the order they are declared. Hence the name "table”.
We can administer the tables through Iptables.

To summarize, Iptables allows defining rules stored in tables that can filter or transform network packets.

Iptables' tables are:

* __Filter__: this is the default and the most used table. It enables firewall functionalities. You can accept or reject selected packets.
* __Nat__: applies Network Address Translation rules. It can modify the packets' address and port for the source and destination.
* __Mangle__: modifies other properties of the packets at the network layer, such as the Time To Live (TTL) or Type Of Service (TOS). It is also able to mark packets at the kernel level. We can use those marks to identify packets in other tables or network processing tools, to apply specific processing.
* __Raw__: Iptables being stateful, it sees the packets as parts of an ongoing connection. The Raw table allows specifying a NOTRACK target so that packets are not linked to a connection. It intervenes first in the Prerouting and Output chains, before Conntrack is applied to packets.
* __Security__: used to set security-related marks on packets at the kernel level. Those marks can then be used by security subsystems such as SELinux.

These tables are applied to a restricted set of the Iptables chains (or Netfilter hooks) based on their nature. The following diagram illustrates the arrangement of the tables within chains.

![Netfilter tables](/posts/iptables_tutorial/netfilter_tables.jpg#medium)

## Writing our first rule

Now, we have a solid mental model of chains and tables organization. We will use it to write our first rule. The easiest way to start is by writing a filter rule that will filter packets based on a matching rule. The filter table is indeed the most commonly used. It fits in the Input, Forward, and Output chains.

![Netfilter possible paths](/posts/iptables_tutorial/filter_tables.jpg#medium)

### Enable SSH traffic on a server

As our first rule, we will enable SSH traffic on a server.
For our case, we assume that there is no restriction on egress (outgoing traffic), and all ingress (incoming traffic) is blocked.
Enabling SSH traffic requires accepting ingress traffic whose target port is the SSH port (usually 22) and using the TCP protocol. Here, we analyze the problem step by step:

1. We must create a filter function to accept specific packets based on matching criteria, i.e., a rule added to the __filter table__.
2. This filter function applies to packets arriving from a network interface to the local process. The prerouting chain has no filter table. Therefore, we must set the rule in the __input chain__ based on the schema above.
3. Packets to be intercepted for an ssh session use the __TCP protocol__ and the __port 22__.
4. The function must __accept__ packets intercepted by our rule.

Using the Iptables command, this translates to:

```bash
#               use filter table    target is accept for matching packets
#                     │                           │
#                 ────┴────                   ────┴────
iptables -A INPUT -t filter -p tcp --dport 22 -j ACCEPT
#        ────┬───           ────────┬────────
#            │                      │
#    add to chain input   match tcp packets on port 22
```

As a side note, we could have used the string ssh instead of 22. The file /etc/services maps services names with their port.

Additionally, we also could have dropped the table flag as _Filter_ is the default:

```bash
iptables -A INPUT -p tcp --dport ssh -j ACCEPT
```

### Some other simple use cases with the input chain and filter table

We can use many more matching options for selecting packets, as we will see in more detail later. But here are some common ones:

```bash
-p protocol          # network protocol
--dport port-number  # destination port
-i name              # network interface that received the packet
-s address[/mask]    # ip or ip range of the packet source
```

The target specified by the flag “-j” can be either Accept, Drop or Reject for the filter table.

```bash
-j {ACCEPT|REJECT|DROP}
```

The difference between Drop and Reject is that Drop does nothing with the packet. The caller is not notified. Drop is a _stealth mode_. But if only your port 22 is on target Drop, it is not stealthy. On the other hand, Reject is the system's default behavior for closed ports. It replies with an ICMP packet with port-unreachable.

Using these basic options, we can already set many useful rules. Here are some simple examples:

```bash
# Blacklisting a specific IP:
iptables -A INPUT -s 11.12.13.14 -j DROP
# Block a range of IPs:
iptables -A INPUT -s 11.12.13.0/24 -j DROP
# Block all ingress traffic:
iptables -A INPUT -j DROP
```

Finally, you can also use negation using the “!” character before some options:

```bash
# Reject all IPs not in 10.0.0.0/8 CIDR block:
iptables -A INPUT ! -s 10.0.0.0/8 -j DROP
```

### Organizing our rules within chains

Creating rules is not sufficient to handle your network packets. You also have to set their position carefully. Because as we said above, a table applies rules to network packets __in the order of the list__. Consequently, the first Accept, Drop, or Reject rule that matches determines the packet's fate.

The following Iptables command flags will allow you to manage rules and their positions, within chains, for a given table.

```bash
# Append the rule to the end of the selected chain
-A chain rule-specification
# Insert the rule at the position rulenum (1 by default)
-I chain [rulenum] rule-specification
# List all rules in the selected chain or all chains
-L [chain]
# Delete rule in specified chain
-D chain {rulenum|rule-specification}
# Flush the chain or all chains (deleting all rules)
-F [chain]
```

To see the rule we created earlier for ssh, we list rules in the INPUT chain.

```bash
iptables -L INPUT --line-numbers
# output
Chain INPUT (policy ACCEPT)
num  target  prot opt source     destination
1    ACCEPT  tcp  --  anywhere   anywhere       tcp dpt:ssh
```

The command lists the filter table rules in the input chain with their associated position number. The filter table is the default one; we did not specify it. We can see the ssh rule created earlier at position 1.

But, what happens when there is no rule?

This is where the table's __default policy__ enters into action. It is stated on the first line of the output, between the parentheses, as “policy ACCEPT”. This policy means that, by default, all packets are accepted in the INPUT chain. Then, in this case, our rule is not needed. However, if we set a real server, we would change this policy to DROP (after enabling the ssh packets).

```bash
# Sets the INPUT chain's default policy to DROP
iptables -P INPUT DROP
```

To delete the ssh rule, we enter:

```bash
iptables -D 1
# or flush the whole chain
iptables -F INPUT
```

## Persisting our rules

By default, Iptables rules are not persistent after reboot.

To persist them, you have several solutions, depending on your Linux distribution. I will only describe the most common ones.

### Debian/Ubuntu

The simplest way for Debian/Ubuntu users is to use the iptables-persistent package. You can install it with the following command:

```bash
apt install iptables-persistent
```

It will automatically save your current configuration at installation time. You can update it later, using the iptables-save package to save your rules in the /etc/iptables/rules.v4 file (for IPv4).

```bash
# To save current rules (for IPv4)
iptables-save > /etc/iptables/rules.v4
```

### RHEL/CentOS

For RHEL/CentOS, the simplest way is to use the iptables-services package.

```bash
# Install the service
yum install iptables-services
# Enable the service to make it start automatically
systemctl enable iptables
# If you change the rules, update the configuration file
iptables-save > /etc/sysconfig/iptables
```

## Conclusion

You are now able to configure Iptables using basic rules. And most importantly, you have a good mental model that will help you organize your rules within relevant tables and chains.

![demo](/posts/iptables_tutorial/vador_meme.jpg#small)
