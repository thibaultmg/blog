+++ 
draft = false
date = 2023-02-16T09:48:30+01:00
title = "Regain control over your network with Iptables — Debugging — Part III"
description = "Third part of the Iptables tutorial series. Learn how to debug Iptables rules." 
slug = "iptables-tutorial-part-three"
authors = []
tags = ["Linux", "Iptables", "network", "firewall", "tutorial"]
categories = []
externalLink = ""
series = ["Iptables tutorial"]
+++

This is the third and last part of the Iptables tutorial series. We have seen how to configure Iptables and use some extensions. However, it can be very tricky to debug network issues caused by Iptables where many rules can be set. What do you do if your network packet is not going through? This is what we will cover here. 

{{< figure src="/posts/iptables_tutorial/debug_illust.jpg#medium" attrlink="https://unsplash.com/fr/photos/M5tzZtFCOfs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText" attr="Taylor Vick" rel="rel" caption="Photo from Unsplash — ">}}

For reference, the series' articles list is at the [bottom]({{< ref "#see-also-in-iptables-tutorial" >}}) of the page.

For this tutorial, I will use the following environment:

* Ubuntu server 22.10
* Kernel 5.19.0-31-generic
* iptables 1.8.7
* nftables 1.0.5

## Creating an invalid rule

Let's start by creating a faulty rule that drops all packets sent to the ip `8.8.8.8`, which is Google's DNS server. We can reuse the diagram from the previous part to understand what we need to do:

![Netfilter filter table](/posts/iptables_tutorial/filter_tables.jpg#medium)

We will add a rule on the `filter` table in the `OUTPUT` chain. The rule will drop all packets to the IP `8.8.8.8` with the protocol `ICMP` (used by the ping command) on the interface `enp0s1` (my network interface connected to the internet):

```bash
# Set the rule
$ iptables -A OUTPUT -p icmp -d 8.8.8.8 -i enp0s1 -j DROP

# List the rules to check that the rule has been added
$ iptables -L -nv
Chain OUTPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 DROP       icmp --  *      enp0s1  0.0.0.0/0            8.8.8.8            
```

Now, let's try to send an ICMP packet with the `ping` command:

```bash
$ ping -c1 -W1 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms
```

No surprise, the ping is not going through. 

Let's assume that we ignore this rule exists. And we may have tens or hundreds of them. How can we identify the rule causing packet loss?

## Tracing to the rescue

Looking at `man iptables-extensions` we can see that there is a special target called `TRACE`:

> This target marks packets so that the kernel will log every rule which matches the packets as those traverse the tables, chains, and rules. It can only be used in the raw table.

The raw table is the first to be processed by Iptables for both the `OUTPUT` and `PREROUTING` chains. It will apply to outgoing packets any other rules, ensuring complete visibility of the packet processing.

Let's use it:

```bash
$ iptables -t raw -A OUTPUT -p icmp -d 8.8.8.8 -j TRACE

$ iptables -t raw -L OUTPUT
Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
TRACE      icmp --  anywhere             anywhere          
```

Our tracing rule is set. Looking at the `man iptables-extensions` again, it states the following:

> With iptables-nft, the target is translated into nftables' meta nftrace expression. Hence the kernel sends trace events via netlink to userspace where they may be displayed using xtables-monitor --trace command.

This means that under the hood, the Linux kernel relies on `nftables` to capture network packets, not anymore on `iptables`. `iptables-nft` is a compatibility layer that allows using `iptables` commands with `nftables` under the hood. For more information and context, you can read this excellent response on [serverfault](https://serverfault.com/questions/1109845/i-cannot-get-iptables-to-show-trace-logs).

Also, note that traces are not logged in the kernel logs but sent directly to the userspace via a netlink socket. So, don't expect to see any logs in `/var/log/kern.log` or even `/var/log/syslog`. We must use the `xtables-monitor` tool to see the traces.

## Analyzing the traces

We launch `xtables-monitor` and see the logs:

```bash
# Start the xtables-monitor
$ xtables-monitor --trace -4

# In another terminal, ping again the ip
$ ping -c1 -W1 8.8.8.8

# In the xtables-monitor terminal, we can see the trace
PACKET: 2 b0b75fd9 OUT=enp0s1 SRC=192.168.86.130 DST=8.8.8.8 LEN=84 TOS=0x0 TTL=64 ID=42090DF 
 TRACE: 2 b0b75fd9 raw:OUTPUT:rule:0x4:CONTINUE  -4 -t raw -A OUTPUT -p icmp -j TRACE
 TRACE: 2 b0b75fd9 raw:OUTPUT:return:
 TRACE: 2 b0b75fd9 raw:OUTPUT:policy:ACCEPT 
 TRACE: 2 b0b75fd9 nat:OUTPUT:return:
 TRACE: 2 b0b75fd9 nat:OUTPUT:policy:ACCEPT 
PACKET: 2 b0b75fd9 OUT=enp0s1 SRC=192.168.86.130 DST=8.8.8.8 LEN=84 TOS=0x0 TTL=64 ID=42090DF 
 TRACE: 2 b0b75fd9 filter:OUTPUT:rule:0x22:DROP  -4 -t filter -A OUTPUT -d 8.8.8.8/32 -o enp0s1 -p icmp -j DROP
```

Line 8 shows the packet we sent with the `ping` command: 

* `2` is the address family number for IPv4 for the socket (AF_INET)
* `b0b75fd9` is the packet identifier that allows correlating messages coming from rules evaluation
* then it shows additional information such as the network interface, the source IP, the destination IP etc.

Line 9 shows the trace for the first rule that matches the packet. It shows the table, the chain, the rule number, the rule itself, and the rule correlation id.

Line 15 is the last rule evaluated for that packet that resulted in a `DROP` action.

Let's see if we can reproduce mentally this rules traversal from iptables, listing the rules of the tables in the `OUTPUT` chain as described in our schema:

![Netfilter tables](/posts/iptables_tutorial/netfilter_tables.jpg#medium)

Following this diagram, we start with the `raw` table, then `nat`, and finally `filter`:

```bash
# List the rules in the raw table
# Only the TRACE rule is listed; it lets the packet go through
# Then the packet follows the default policy of the table that is ACCEPT
# The packet then goes to the nat table
$ iptables -t raw -L OUTPUT -vn
Chain OUTPUT (policy ACCEPT 11 packets, 929 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    1    84 TRACE      icmp --  *      *       0.0.0.0/0            0.0.0.0/0           

# List the rules in the nat table
# Only the default policy is listed; it lets the packet go through
# The packet then goes to the filter table
$ iptables -t nat -L OUTPUT -vn
Chain OUTPUT (policy ACCEPT 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         

# List the rules in the filter table
# The DROP rule is listed for destination `8.8.8.8` is listed
# The packet is then captured and dropped
$ iptables -t filter -L OUTPUT -vn
Chain OUTPUT (policy ACCEPT 60 packets, 7962 bytes)
 pkts bytes target     prot opt in     out     source               destination         
    3   252 DROP       icmp --  *      enp0s1  0.0.0.0/0            8.8.8.8  
```

It seems that the trace reflects our rules. There is though one last thing that appears in the traces that we haven't talked about. What is this `0x22` in the `filter:OUTPUT:rule:0x22:DROP` trace?

## Finding the identified rule and removing it

`0x22` is the hexadecimal identifier of the rule, which translates to `34` in decimal. However, that identifier is never seen when we list the rules with `iptables -L --line-numbers`. That is unfortunate because if it was the `iptables` rule number, we could identify the rule more easily. 

Well, do you remember what we said about `iptables` and `nftables`? `nftables` is the new implementation of `iptables`, and it is the one that is used under the hood. So, the `--line-numbers` option is not supported by `nftables` tracing. However, there is a way to get the identifier of the rule. We can use the `nft` command:

```bash
# List the rules as seen by nftables with the nft command that replaces iptables
# Restrict the output to the rule that we are interested in
$ nft -a list ruleset | grep 34 -A2 -B2
chain OUTPUT { # handle 33
                type filter hook output priority 0; policy accept;
                oifname "enp0s1" meta l4proto 1 ip daddr 8.8.8.8 counter packets 3 bytes 252 drop # handle 34
        }
}
```

Here it is. We found the rule matching the trace. The `nft` command can then reference the rule by its identifier. We can also use the `nft` command to delete the rule:

```bash
# Delete the rule using the handle identifier
$ nft delete rule filter OUTPUT handle 34

# List the rules again, the rule is gone
$ nft -a list ruleset | grep 34

# List the rules with iptables, the rule is gone
$ iptables -t filter -L OUTPUT
Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination        

# Ping again the ip, the packet is not dropped anymore!
$ ping -c1 -W1 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=116 time=36.8 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 36.784/36.784/36.784/0.000 ms
```

## Cleanup

All is working again. Let's clean up the rules that we created:

```bash
# Delete the rules that we created
$ iptables -t raw -L OUTPUT -n --line-numbers
Chain OUTPUT (policy ACCEPT)
num  target     prot opt source               destination         
1    TRACE      icmp --  0.0.0.0/0             8.8.8.8             limit: avg 1/min burst 1

$ iptables -t raw -D OUTPUT 1

$ iptables -t raw -L OUTPUT --line-numbers
Chain OUTPUT (policy ACCEPT)
num  target     prot opt source               destination    
```

## Limit the number of packets

In real life, you might be working on a busy server, and you don't want to flood the logs with too many traces. You can limit the number of packets that match our rule using the `limit` extension. For our example, it would look like this:

```bash
# Limit the number of packets to 1 per minute
$ iptables -t raw -A OUTPUT -p icmp -d 8.8.8.8 -m limit --limit 1/min --limit-burst 1 -j TRACE
```

## Conclusion

This is the end of our Iptables tutorials series. We covered a lot of ground, from building a mental model to creating our own chains and troubleshooting. I hope you enjoyed it and feel in control of your network traffic!

In a future post, I will cover the `nftables` command that is the current implementation and replaces `iptables`. But don't worry, as you have seen, we can still use the `iptables` command.

For writing this post, I used the following resources:

* A comment from [serverfault](https://serverfault.com/questions/1109845/i-cannot-get-iptables-to-show-trace-logs) that provides some details on the move from `iptables` to `nftables`
* `man iptables`
* `man nft`
* `man xtables-monitor`

