---
title: "About"
date: 2021-12-18T15:25:38+01:00
draft: false
---

Hi Web traveler, my name is Thibault.

Thanks for stopping by. The articles you will find here are about software engineering, technology, and the technical books I read. Professionally, I am a backend developer. 

Please, do not hesitate to contact me for feedback and comments. They are always appreciated!

I hope you will enjoy the content of this blog and wish you to learn a few useful things.
